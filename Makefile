CC ?= gcc
CFLAGS = -Wall -Wextra -Werror -std=c99 -pedantic

SRC = src/*.c
BIN = myfind

all: $(BIN)

$(BIN): $(SRC)
	$(CC) $(CFLAGS) -o $@ $^

check: $(BIN)
	touch myfind_output.txt find_output.txt
	$(CC) $(CFLAGS) -o test tests/test.c

clean:
	$(RM) $(BIN) *.txt test

.PHONY: clean
