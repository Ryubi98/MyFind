#define _GNU_SOURCE

#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>

int main(int argc, char *argv[])
{

    if (execvp("tests/test.sh", argv) == -1)
    {
        fprintf(stderr, "test: cannot do execvp: an error occured\n");
        return 1;
    }

    return argc;
}
