#ifndef EXPRESSION_H
#define EXPRESSION_H

#define _GNU_SOURCE

#include <stdio.h>
#include <sys/stat.h>

#include "my_string.h"
#include "arguments.h"

void apply_expression(char *arg, char *name_file, struct stat s,
                      struct expression *exp);

#endif /* !EXPRESSION_H */
