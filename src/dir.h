#ifndef DIR_H
#define DIR_H

#define _GNU_SOURCE

#include <dirent.h>
#include <sys/stat.h>

#include "my_string.h"
#include "arguments.h"
#include "expression.h"

int my_find(char *file, char *arg, char *name, struct options *options,
            struct expression *exp);

#endif /* !DIR_H */
