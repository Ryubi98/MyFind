#ifndef MY_STRING_H
#define MY_STRING_H

#include <stdio.h>
#include <stdlib.h>

int my_strlen(char *s);

int my_strequal(char *s1, char *s2, int n);

char *my_strcpy(char *source);

char *check_path(char *s);

char *concatenate(char *s1, char *s2);

#endif /* !MY_STRING_H */
