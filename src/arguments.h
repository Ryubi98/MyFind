#ifndef ARGUMENTS_H
#define ARGUMENTS_H

#include <stdio.h>
#include <stdlib.h>

#include "my_string.h"

enum order
{
    PRE_ORDER = 0,
    POST_ORDER
};

enum option
{
    P_OPTION = 0,
    L_OPTION,
    H_OPTION
};

struct options
{
    enum order ord;
    enum option opt;
};

struct files
{
    int start;
    int end;
};

enum function
{
    PRINT = 0,
    NAME,
    TYPE
};

struct expression
{
    enum function f;
    char *arg;
};

int check_arg(int argc, char *argv[], struct files **f, struct options **o,
              struct expression **e);

#endif /* !ARGUMENTS_H */
