#include <stdio.h>
#include <stdlib.h>

#include "dir.h"
#include "my_string.h"
#include "arguments.h"

int main(int argc, char *argv[])
{
    struct options *options = NULL;
    struct files *files = NULL;
    struct expression *exp = NULL;

    if (check_arg(argc, argv, &files, &options, &exp))
    {
        fprintf(stderr, "myfind: cannot do parsing: unknow command\n");
        return 1;
    }

    int res = 0;
    if (files->start == files->end)
    {
        if (my_find(".", ".", ".", options, exp))
            res = 1;
    }
    else
    {
        for (int i = files->start; i < files->end; i++)
        {
            if (my_find(argv[i], argv[i], argv[i], options, exp))
                res = 1;
        }
    }

    free(options);
    free(files);
    free(exp);

    return res;
}
