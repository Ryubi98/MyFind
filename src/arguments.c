#include "arguments.h"

static struct options *alloc_options(void)
{
    struct options *options = calloc(1, sizeof(*options));
    if (!options)
    {
        fprintf(stderr, "myfind: cannot do allocation: calloc failed\n");
        exit(1);
    }

    return options;
}

static struct files *alloc_files(struct options *options)
{
    struct files *files = calloc(1, sizeof(*files));
    if (!files)
    {
        free(options);
        fprintf(stderr, "myfind: cannot do allocation: calloc failed\n");
        exit(1);
    }

    return files;
}

static void free_alloc(struct files *files, struct options *options,
                       struct expression *exp)
{
    free(files);
    free(options);
    free(exp);
}

static int check_options(char *arg, struct options *options)
{
    if (my_strequal(arg, "-d\0", 3))
        options->ord = POST_ORDER;
    else if (my_strequal(arg, "-H\0", 3))
        options->opt = H_OPTION;
    else if (my_strequal(arg, "-L\0", 3))
        options->opt = L_OPTION;
    else if (my_strequal(arg, "-P\0", 3))
        options->opt = P_OPTION;
    else
        return 1;

    return 0;
}

static struct expression *alloc_expressions(struct options *options,
                                            struct files *files)
{
    struct expression *exp = calloc(1, sizeof(*exp));
    if (!exp)
    {
        free(options);
        free(files);
        fprintf(stderr, "myfind: cannot do allocation: calloc failed\n");
        exit(1);
    }

    return exp;
}

static int check_types(char *arg, struct expression *exp)
{
    if (my_strequal(arg, "b\0", 2))
        exp->arg = arg;
    else if (my_strequal(arg, "c\0", 2))
        exp->arg = arg;
    else if (my_strequal(arg, "d\0", 2))
        exp->arg = arg;
    else if (my_strequal(arg, "f\0", 2))
        exp->arg = arg;
    else if (my_strequal(arg, "l\0", 2))
        exp->arg = arg;
    else if (my_strequal(arg, "p\0", 2))
        exp->arg = arg;
    else if (my_strequal(arg, "s\0", 2))
        exp->arg = arg;
    else
        return 1;

    return 0;
}

static int check_expressions(char *argv[], int argc, int i,
                             struct expression *exp)
{
    if (my_strequal(argv[i], "-print\0", 7) && argc == i + 1)
        exp->f = PRINT;
    else if (my_strequal(argv[i], "-name\0", 6) && argc > i + 1)
    {
        exp->f = NAME;
        exp->arg = argv[i + 1];
    }
    else if (my_strequal(argv[i], "-type\0", 6) && argc > i + 1)
    {
        exp->f = TYPE;
        if (check_types(argv[i + 1], exp))
            return 1;
    }
    else
        return 1;

    return 0;
}

int check_arg(int argc, char *argv[], struct files **f, struct options **o,
              struct expression **e)
{
    struct options *options = alloc_options();
    struct files *files = alloc_files(options);
    struct expression *exp = alloc_expressions(options, files);

    int i = 1;

    for (; i < argc; i++)
    {
        if (argv[i][0] != '-')
            break;

        if (check_options(argv[i], options))
            break;
    }

    files->start = i;
    for (; i < argc; i++)
    {
        if (argv[i][0] == '-')
            break;
    }
    files->end = i;

    for (; i < argc - 1; i++)
    {
        if (check_expressions(argv, argc, i, exp))
        {
            free_alloc(files, options, exp);
            return 1;
        }
    }

    *o = options;
    *f = files;
    *e = exp;
    return 0;
}
