#include "expression.h"

static void print(char *arg)
{
    printf("%s\n", arg);
}

static void name(char *arg, char *name_file, struct expression *exp)
{
    if (my_strequal(name_file, exp->arg, my_strlen(name_file)))
        printf("%s\n", arg);
}

static void type(char *arg, struct stat s, struct expression *exp)
{
    if (my_strequal(exp->arg, "b\0", 2))
    {
        if ((s.st_mode & S_IFMT) == S_IFBLK)
            printf("%s\n", arg);
    }
    else if (my_strequal(exp->arg, "c\0", 2))
    {
        if ((s.st_mode & S_IFMT) == S_IFCHR)
            printf("%s\n", arg);
    }
    else if (my_strequal(exp->arg, "d\0", 2))
    {
        if ((s.st_mode & S_IFMT) == S_IFDIR)
            printf("%s\n", arg);
    }
    else if (my_strequal(exp->arg, "f\0", 2))
    {
        if ((s.st_mode & S_IFMT) == S_IFREG)
            printf("%s\n", arg);
    }
    else if (my_strequal(exp->arg, "l\0", 2))
    {
        if ((s.st_mode & S_IFMT) == S_IFLNK)
            printf("%s\n", arg);
    }
    else if (my_strequal(exp->arg, "p\0", 2))
    {
        if ((s.st_mode & S_IFMT) == S_IFIFO)
            printf("%s\n", arg);
    }
    else if (my_strequal(exp->arg, "s\0", 2))
    {
        if ((s.st_mode & S_IFMT) == S_IFSOCK)
            printf("%s\n", arg);
    }
}

void apply_expression(char *arg, char *name_file, struct stat s,
                      struct expression *exp)
{
    switch (exp->f)
    {
    case PRINT:
        print(arg);
    break;
    case NAME:
        name(arg, name_file, exp);
    break;
    case TYPE:
        type(arg, s, exp);
    break;
    }
}
