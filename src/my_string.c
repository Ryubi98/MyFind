#include "my_string.h"

int my_strlen(char *s)
{
    int len = 0;

    while (*(s + len))
        len++;

    return len + 1;
}

int my_strequal(char *s1, char *s2, int n)
{
    for (int i = 0; i < n; i++)
    {
        if (*(s1 + i) != *(s2 + i))
            return 0;

        if (!*(s1 + i) || !*(s2 + i))
            break;
    }

    return *(s1 + n - 1) == *(s2 + n - 1);
}

char *my_strcpy(char *source)
{
    int len = my_strlen(source);

    char *res = malloc(sizeof(char) * len);
    if (!res)
    {
        fprintf(stderr, "myfind: cannot do allocation: malloc failed\n");
        return NULL;
    }

    for (int i = 0; i < len; i++)
        *(res + i) = *(source + i);

    return res;
}

char *check_path(char *s)
{
    int len = my_strlen(s);

    char *res = my_strcpy(s);
    if (!res)
        return NULL;

    if (*(res + len - 2) != '/')
    {
        char *new = realloc(res, sizeof(char) * (len + 1));
        if (!new)
        {
            free(res);
            fprintf(stderr, "myfind: cannot do allocation: realloc failed\n");
            return NULL;
        }

        res = new;

        *(res + len - 1) = '/';
        *(res + len) = '\0';
    }

    return res;
}

char *concatenate(char *s1, char *s2)
{
    int len_s1 = my_strlen(s1);
    int len_s2 = my_strlen(s2);

    char *res = malloc(sizeof(char) * (len_s1 + len_s2));
    if (!res)
    {
        fprintf(stderr, "myfind: cannot do allocation: malloc failed\n");
        return NULL;
    }

    for (int i = 0; i < len_s1 - 1; i++)
        *(res + i) = *(s1 + i);
    for (int j = 0; j < len_s2; j++)
        *(res + len_s1 - 1 + j) = *(s2 + j);

    return res;
}
