#include "dir.h"

static DIR *open_dir(char *path)
{
    DIR *res = opendir(path);
    if (!res)
    {
        fprintf(stderr, "myfind: cannot do opendir: %s: ", path);
        fprintf(stderr, "This is not a directory\n");
        return NULL;
    }

    return res;
}

static int check_dir(char *file, char *arg, struct stat s,
                     struct options *options)
{
    if (S_ISLNK(s.st_mode))
    {
        if (options->opt == P_OPTION)
            return 0;
        else if (options->opt == H_OPTION)
        {
            if (my_strequal(file, arg, my_strlen(file)))
                return 1;
            else
                return 0;
        }
        else
            return 1;
    }
    else if (S_ISDIR(s.st_mode))
        return 1;
    else
        return 0;
}

static int check_stat(char *arg, struct stat *s)
{
    if (lstat(arg, s))
    {
        fprintf(stderr, "myfind: cannot do lstat: no file or directory\n");
        return 1;
    }

    return 0;
}

int my_find(char *file, char *arg, char *name, struct options *options,
            struct expression *exp)
{
    struct stat s;
    if (check_stat(arg, &s))
        return 1;

    if (options->ord == PRE_ORDER)
        apply_expression(arg, name, s, exp);

    if (check_dir(file, arg, s, options))
    {
        char *path = check_path(arg);

        DIR *dir = open_dir(path);
        if (!dir)
        {
            free(path);
            return 1;
        }

        struct dirent *entry = readdir(dir);
        for (; entry; entry = readdir(dir))
        {
            if (!my_strequal("..\0", entry->d_name, 3))
            {

                if (!my_strequal(".\0", entry->d_name, 2))
                {
                    char *sub_path = concatenate(path, entry->d_name);
                    if (!sub_path)
                        continue;

                    my_find(file, sub_path, entry->d_name, options, exp);
                    free(sub_path);
                }
            }
        }
        closedir(dir);
        free(path);
    }

    if (options->ord == POST_ORDER)
        apply_expression(arg, name, s, exp);

    return 0;
}
